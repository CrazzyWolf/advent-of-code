﻿namespace Advent_of_Code
{
    internal class Day1
    {
        public static void Start()
        {
            string[] Lines = File.ReadAllLines("Day1/input.tar");

            int Max1 = 0;
            int Max2 = 0;
            int Max3 = 0;
            int Buffer = 0;

            foreach(string Line in Lines)
            {
                if(Line.Trim().Length == 0)
                {
                    if(Max1 < Buffer)
                    {
                        Max3 = Max2;
                        Max2 = Max1;
                        Max1 = Buffer;
                    }
                    else if(Max2 < Buffer)
                    {
                        Max3 = Max2;
                        Max2 = Buffer;
                    }
                    else if(Max3 < Buffer)
                    {
                        Max3 = Buffer;
                    }
                    Buffer = 0;
                }
                else
                {
                    Buffer += int.Parse(Line.Trim());
                }
            }

            Console.WriteLine(Max1);
            Console.WriteLine(Max2);
            Console.WriteLine(Max3);
            Console.WriteLine(Max1 + Max2 + Max3);
            Console.ReadLine();
        }
    }
}
