﻿namespace Advent_of_Code
{
    internal class Day15
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("Day15/input.tar");
            List<(int x, int y, int radius)> sensors = new();
            HashSet<(int xBeacon, int yBeacon)> beacons = new();

            foreach(string line in input)
            {
                if(line.Length == 0)
                {
                    break;
                }

                string[] parts = line.Split('=');
                int xSensor = int.Parse(parts[1][..parts[1].IndexOf(',')]);
                int ySensor = int.Parse(parts[2][..parts[2].IndexOf(':')]);
                int xBeacon = int.Parse(parts[3][..parts[3].IndexOf(',')]);
                int yBeacon = int.Parse(parts[4]);
                int radius = Math.Abs(xSensor - xBeacon) + Math.Abs(ySensor - yBeacon);

                sensors.Add((xSensor, ySensor, radius));
                beacons.Add((xBeacon, yBeacon));
            }

            Console.WriteLine(ScannedPositions(sensors, beacons, 2000000));
            Console.WriteLine(FindFrequency(sensors, 4000000));
        }

        private static long FindFrequency(List<(int x, int y, int radius)> Sensors, int Limit)
        {
            for(int row = 0; row <= Limit; row++)
            {
                int start, end;
                Queue<(int rangeStart, int rangeEnd)> ranges = new();
                Queue<(int rangeStart, int rangeEnd)> tempRanges = new();

                foreach((int x, int y, int radius) in Sensors)
                {
                    if(row <= y + radius && row >= y - radius)
                    {
                        int yDiff = radius - Math.Abs(row - y);
                        start = Math.Max(0, x - yDiff);
                        end = Math.Min(Limit, (x - yDiff) + yDiff * 2);

                        if(start <= Limit && end >= 0)
                        {
                            ranges.Enqueue((start, end));
                        }
                    }
                }

                Random random = new();
                (int finalStart, int finalEnd) = ranges.Dequeue();
                int finalStart2, finalEnd2;
                while(true)
                {
                    if(ranges.Count < 1)
                    {
                        if(tempRanges.Count <= 1)
                        {
                            break;
                        }
                        else
                        {
                            ranges.Enqueue(tempRanges.Dequeue());
                        }
                    }

                    (finalStart2, finalEnd2) = ranges.Dequeue();

                    if(random.Next(0, 10) == 0)
                    {
                        tempRanges = new(tempRanges.Reverse());
                    }

                    if(finalStart2 - 1 > finalEnd || finalStart - 1 > finalEnd2)
                    {
                        tempRanges.Enqueue((finalStart, finalEnd));
                        (finalStart, finalEnd) = (finalStart2, finalEnd2);
                    }
                    else
                    {
                        finalEnd = Math.Max(finalEnd, finalEnd2);
                        finalStart = Math.Min(finalStart, finalStart2);
                    }

                    if(finalStart == 0 && finalEnd == Limit)
                    {
                        tempRanges.Clear();
                        break;
                    }
                }

                if(tempRanges.Count > 0)
                {
                    (finalStart2, finalEnd2) = tempRanges.Dequeue();
                    if(finalStart2 - 1 > finalEnd || finalStart - 1 > finalEnd2)
                    {
                        return (finalEnd < finalStart2 ? (finalEnd + 1) : (finalEnd2 + 1)) * 4000000L + row;
                    }
                }
                else if(finalStart != 0 || finalEnd != Limit)
                {
                    return (finalStart == 0 ? Limit : 0) * 4000000L + row;
                }
            }

            return -1;
        }

        private static int ScannedPositions(List<(int x, int y, int radius)> Sensors, HashSet<(int xBeacon, int yBeacon)> Beacons, int Row)
        {
            HashSet<int> positions = new();

            foreach((int x, int y, int radius) in Sensors)
            {
                if(Row <= y + radius && Row >= y - radius)
                {
                    int yDiff = radius - Math.Abs(Row - y);
                    int start = x - yDiff;
                    int end = start + yDiff * 2;

                    for(int i = start; i <= end; i++)
                    {
                        positions.Add(i);
                    }
                }
            }

            foreach((int xBeacon, int yBeacon) in Beacons)
            {
                if(yBeacon == Row)
                {
                    positions.Remove(xBeacon);
                }
            }

            return positions.Count;
        }
    }
}
