﻿namespace Advent_of_Code
{
    internal class Day11
    {
        public static void Start()
        {
            string[] instructions = File.ReadAllLines("Day11/input.txt");

            MonkeBussiness(instructions, 20, 3);
            MonkeBussiness(instructions, 10000, 1);
        }

        private static void MonkeBussiness(string[] Instructions, int Rounds, int WorryDivisor)
        {
            int numOfMonke = (Instructions.Length + 1) / 7;
            Queue<long>[] items = new Queue<long>[numOfMonke];
            string[][] operations = new string[numOfMonke][];
            long[] inspectingCounters = new long[numOfMonke];
            int[,] tests = new int[numOfMonke, 3];
            int moduloLimit = 1;

            for(int i = 0; i < numOfMonke; i++)
            {
                int index = i * 7;
                items[i] = new Queue<long>(Array.ConvertAll(Instructions[index + 1].Split(": ")[1].Split(", "), s => long.Parse(s)));
                operations[i] = Instructions[index + 2].Split("= ")[1].Split(" ");
                moduloLimit *= tests[i, 0] = ParseTest(Instructions[index + 3]);
                tests[i, 1] = ParseTest(Instructions[index + 4]);
                tests[i, 2] = ParseTest(Instructions[index + 5]);
            }

            for(int round = 1; round <= Rounds; round++)
            {
                for(int i = 0; i < numOfMonke; i++)
                {
                    Queue<long> monkeItems = items[i];
                    int count = monkeItems.Count;
                    string[] operation = operations[i];

                    for(int j = 0; j < count; j++)
                    {
                        long worry = monkeItems.Dequeue();
                        long item1 = GetItem(worry, operation[0]);
                        long item2 = GetItem(worry, operation[2]);
                        long result = (operation[1] == "*" ? item1 * item2 : item1 + item2) / WorryDivisor % moduloLimit;
                        items[result % tests[i, 0] == 0 ? tests[i, 1] : tests[i, 2]].Enqueue(result);
                        inspectingCounters[i]++;
                    }
                }
            }

            long monke1 = 0;
            long monke2 = 0;

            foreach(long counter in inspectingCounters)
            {
                if(counter > monke1)
                {
                    monke2 = monke1;
                    monke1 = counter;
                }
                else if(counter > monke2)
                {
                    monke2 = counter;
                }
            }

            Console.WriteLine(monke1 * monke2);
        }

        private static long GetItem(long Worry, string Text)
        {
            return Text == "old" ? Worry : long.Parse(Text);
        }

        private static int ParseTest(string instruction)
        {
            int i = instruction.LastIndexOf(" ");
            return int.Parse(instruction[(i + 1)..]);
        }
    }
}
