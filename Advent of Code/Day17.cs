﻿namespace Advent_of_Code
{
    internal class Day17
    {
        private readonly static List<bool[]> map = new();
        private readonly static int[] rockYOffsets = new int[] { 0, 2, 2, 3, 1 };
        private readonly static (int y, int x)[][] rockTypes = new (int, int)[][]
        {
            new[] { (0, -1), (0, 2), (0, 0), (0, 1) },
            new[] { (1, -1), (1, 1), (0, 0), (1, 0), (2, 0) },
            new[] { (0, -1), (2, 1), (0, 1), (1, 1), (0, 0) },
            new[] { (0, -1), (1, -1), (2, -1), (3, -1) },
            new[] { (0, -1), (0, 0), (1, -1), (1, 0) },
        };

        public static void Start()
        {
            char[] pattern = File.ReadAllLines("Day17/input.txt")[0].ToArray();
            Simulate(pattern, 2022);
            PrintMap();
            Console.WriteLine(map.Count);
            Console.WriteLine(Simulate(pattern, 1000000000000));
        }

        private static long Simulate(char[] pattern, long length)
        {
            int jet = 0;
            Dictionary<(int, int), (long, int)> dict = new();
            map.Clear();

            for(long step = 0; step < length; step++)
            {
                int rockType = (int)(step % rockTypes.Length);

                (int, int) key = (rockType, jet);
                int top = map.Count;
                if(dict.ContainsKey(key))
                {
                    (long s, int t) = dict[key];

                    long a = length - step;
                    long b = step - s;

                    if(a % b == 0)
                    {
                        return top + (top - t) * (a / b);
                    }
                }
                else
                {
                    dict.Add(key, (step, top));
                }

                ExpandMap(rockType);
                (int y, int x)[] rockPos = rockTypes[rockType];
                (int y, int x) = (map.Count - rockYOffsets[rockType], 3);

                while(true)
                {
                    y--;

                    if(pattern[jet] == '<')
                    {
                        if(rockPos[0].x + x > 0 && !CheckBellow(y, x, rockPos, 0, -1))
                        {
                            x--;
                        }
                    }
                    else
                    {
                        if(rockPos[1].x + x < 6 && !CheckBellow(y, x, rockPos, 0, 1))
                        {
                            x++;
                        }
                    }

                    jet = ++jet % pattern.Length;

                    if(y == 0 || CheckBellow(y, x, rockPos, -1, 0))
                    {
                        foreach(var r in rockPos)
                        {
                            map[y + r.y][x + r.x] = true;
                        }

                        int shrink = ShrinkMap();
                        map.RemoveRange(map.Count - shrink, shrink);

                        break;
                    }
                }
            }

            return map.Count;
        }

        private static bool CheckBellow(int y, int x, (int y, int x)[] rockPos, int yOffset, int xOffset)
        {
            foreach(var r in rockPos)
            {
                if(map[r.y + y + yOffset][r.x + x + xOffset])
                {
                    return true;
                }
            }

            return false;
        }

        private static void ExpandMap(int rockType)
        {
            int y = 4 + rockYOffsets[rockType];
            for(int i = 0; i < y; i++)
            {
                map.Add(new bool[7]);
            }
        }

        private static int ShrinkMap()
        {
            int length = 0;
            for(int i = map.Count - 1; i >= 0; i--)
            {
                for(int j = 0; j < 7; j++)
                {
                    if(map[i][j])
                    {
                        return length;
                    }
                }
                length++;
            }
            return length;
        }

        private static void PrintMap((int y, int x, (int, int)[] rockPos) ?pos = null)
        {
            List<bool[]> tempMap = new();

            foreach(var row in map.GetRange(map.Count - 20, 20))
            {
                tempMap.Add((bool[])row.Clone());
            }

            if(pos != null)
            {
                foreach(var (y, x) in pos.Value.rockPos)
                {
                    tempMap[pos.Value.y + y][pos.Value.x + x] = true;
                }
            }

            tempMap.Reverse();
            foreach(bool[] row in tempMap)
            {
                Console.WriteLine('|' + string.Join("", row.Select(r => r ? '#' : '.')) + '|');
            }
            Console.WriteLine("+-------+");
        }
    }
}
