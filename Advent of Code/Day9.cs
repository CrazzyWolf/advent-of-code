﻿namespace Advent_of_Code
{
    internal class Day9
    {
        public static void Start()
        {
            string[] moves = File.ReadAllLines("Day9/input.tar");

            (int, int) head = (0, 0);
            (int, int) headPrev = (0, 0);
            (int, int) tail = (0, 0);
            (int, int)[] bigTail = new (int, int)[9];
            HashSet<string> positions = new();
            HashSet<string> positions2 = new();

            void Right() => head.Item1++;
            void Left() => head.Item1--;
            void Up() => head.Item2++;
            void Down() => head.Item2--;

            foreach(string s in moves)
            {
                string[] split = s.Split(" ");
                int length = int.Parse(split[1]);
                Action move = split[0] switch
                {
                    "U" => Up,
                    "D" => Down,
                    "R" => Right,
                    "L" => Left,
                    _ => throw new Exception("Wrong input"),
                };

                for(int i = 0; i < length; i++)
                {
                    move(); 

                    if(Math.Abs(head.Item1 - tail.Item1) > 1 || Math.Abs(head.Item2 - tail.Item2) > 1)
                    {
                        tail = headPrev;
                        positions.Add(tail.Item1 + "," + tail.Item2);
                    }

                    for(int j = 0; j < bigTail.Length; j++)
                    {
                        (int, int) knot = bigTail[j];
                        (int, int) prevKnot = j == 0 ? head : bigTail[j - 1];

                        bool item1IsSmaller = knot.Item1 < prevKnot.Item1;
                        bool item2IsSmaller = knot.Item2 < prevKnot.Item2;

                        if(Math.Abs(knot.Item1 - prevKnot.Item1) > 1)
                        {
                            knot.Item1 += item1IsSmaller ? 1 : -1;
                            knot.Item2 += item2IsSmaller ? 1 : knot.Item2 > prevKnot.Item2 ? -1 : 0;
                        }
                        else if(Math.Abs(knot.Item2 - prevKnot.Item2) > 1)
                        {
                            knot.Item1 += item1IsSmaller ? 1 : knot.Item1 > prevKnot.Item1 ? -1 : 0;
                            knot.Item2 += item2IsSmaller ? 1 : -1;
                        }

                        bigTail[j] = knot;
                    }

                    (int, int) last = bigTail[^1];
                    positions2.Add(last.Item1 + "," + last.Item2);

                    headPrev = head;
                }
            }

            Console.WriteLine(positions.Count);
            Console.WriteLine(positions2.Count);
        }
    }
}