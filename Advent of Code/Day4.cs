﻿namespace Advent_of_Code
{
    internal class Day4
    {
        public static void Start()
        {
            int sum = 0;

            foreach(string line in File.ReadAllLines("Day4/input.txt"))
            {
                int[] ints = ConvertToInts(line);
                if(ints[0] <= ints[2] && ints[1] >= ints[3] || ints[0] >= ints[2] && ints[1] <= ints[3])
                {
                    sum++;
                }
            }

            Console.WriteLine(sum);
        }

        public static void StartShort()
        {
            Console.WriteLine(File.ReadAllLines("Day4/input.txt").Select(l => l.Split(',', '-').Select(int.Parse).ToArray()).Count(i => i[0] <= i[2] && i[1] >= i[3] || i[0] >= i[2] && i[1] <= i[3]));
        }

        public static void StartShort2()
        {
            Console.WriteLine(File.ReadAllLines("Day4/input.txt").Select(l => l.Split(',', '-').Select(int.Parse).ToArray()).Count(i => i[1] >= i[2] && i[3] >= i[0]));
        }

        private static int[] ConvertToInts(string line)
        {
            return Array.ConvertAll(line.Split(new char[] { ',', '-' }), s => int.Parse(s));
        }

        public static void Start2()
        {
            int sum = 0;

            foreach(string line in File.ReadAllLines("Day4/input.txt"))
            {
                int[] ints = ConvertToInts(line);
                if(ints[1] >= ints[2] && ints[3] >= ints[0])
                {
                    sum++;
                }
            }

            Console.WriteLine(sum);
        }
    }
}
