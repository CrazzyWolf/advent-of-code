﻿namespace Advent_of_Code
{
    internal class Day10
    {
        public static void Start()
        {
            string[] instructions = File.ReadAllLines("Day10/input.txt");

            int x = 1;
            int cycle = 0;
            int modulo = 0;
            int sum = 0;
            bool adding = false;
            string output = "";

            for(int i = 0; i < instructions.Length; i++)
            {
                cycle++;
                output += modulo - 2 < x && modulo + 2 > x ? '#' : '.';

                if((modulo = cycle % 40) == 0)
                {
                    output += '\n';
                }
                else if(modulo == 20)
                {
                    sum += cycle * x;
                }

                string instruction = instructions[i];
                if(instruction != "noop")
                {
                    if(adding = !adding)
                    {
                        i--;
                    }
                    else
                    {
                        x += int.Parse(instruction.Split(' ')[1]);
                    }
                }
            }

            Console.WriteLine(sum);
            Console.WriteLine(output);
        }
    }
}
