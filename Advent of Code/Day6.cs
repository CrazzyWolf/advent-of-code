﻿namespace Advent_of_Code
{
    internal class Day6
    {
        public static void Start()
        {
            string datastream = File.ReadAllLines("Day6/input.tar").First();
            Console.WriteLine(StartOfDistinctSequence(datastream, 4));
            Console.WriteLine(StartOfDistinctSequence(datastream, 14));
        }

        private static int StartOfDistinctSequence(string datastream, int Lenght)
        {
            List<char> buffer = new(Lenght);
            int counter = 0;

            foreach(char c in datastream)
            {
                buffer.Add(c);
                counter++;

                if(buffer.Count == Lenght)
                {
                    for(int i = 0; i < Lenght - 1; i++)
                    {
                        char c2 = buffer[i];
                        for(int j = i + 1; j < Lenght; j++)
                        {
                            if(c2 == buffer[j])
                            {
                                buffer.RemoveRange(0, i + 1);
                                goto yeet;
                            }
                        }
                    }

                    return counter;
                }
                yeet:;
            }

            throw new Exception("No sequence found");
        }
    }
}
