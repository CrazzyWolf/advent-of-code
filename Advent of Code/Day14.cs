﻿namespace Advent_of_Code
{
    internal class Day14
    {
        enum Entity
        {
            Air, SandSource, Sand, Rock
        }

        public static void Start()
        {
            string[] input = File.ReadAllLines("Day14/input.tar");

            (int x, int y) sandSource = (500, 0);
            (int xMin, int yMin, int xMax, int yMax) boundaries = (sandSource.x, sandSource.y, sandSource.x, sandSource.y);
            (int x, int y)[][] points = new(int, int)[input.Length][];

            for(int i = 0; i < input.Length; i++)
            {
                string[] split = input[i].Split(" -> ");
                (int x, int y)[] pointsLine = points[i] = new (int, int)[split.Length];
                for(int j = 0; j < split.Length; j++)
                {
                    string[] split2 = split[j].Split(',');
                    (int x, int y) = pointsLine[j] = (int.Parse(split2[0]), int.Parse(split2[1]));
                    boundaries = (Math.Min(boundaries.xMin, x), Math.Min(boundaries.yMin, y),
                        Math.Max(boundaries.xMax, x), Math.Max(boundaries.yMax, y));
                }
            }

            (int xOffset, int yOffset) = (boundaries.xMin, boundaries.yMin);
            sandSource = (sandSource.x - xOffset, sandSource.y - yOffset);
            Entity[,] map = new Entity[boundaries.xMax - xOffset + 1, boundaries.yMax - yOffset + 1];

            for(int i = 0; i < points.Length; i++)
            {
                (int x, int y)[] line = points[i];

                for(int j = 0; j < line.Length; j++)
                {
                    (int x, int y) = line[j];
                    line[j] = (x - xOffset, y - yOffset);
                }

                for(int j = 0; j < line.Length - 1; j++)
                {
                    (int x1, int y1) = line[j];
                    (int x2, int y2) = line[j + 1];

                    bool isXLine = y1 == y2;
                    AddLine(map, isXLine ? (x1, x2) : (y1, y2), isXLine ? y1 : x1, isXLine);
                }
            }

            map[sandSource.x, sandSource.y] = Entity.SandSource;
            map = ExpandMap(map, 0, 2);
            PrintMap(map);
            Console.WriteLine(SimulateSand(map, sandSource, false));
            Console.WriteLine(SimulateSand(map, sandSource, true));
        }

        private static int SimulateSand(Entity[,] Map, (int x, int y) SandSource, bool Expand)
        {
            int sandUnits = 0;
            (int x, int y) currPos = SandSource;
            (int width, int height) = (Map.GetLength(0), Map.GetLength(1));
            while(true)
            {
                //Console.WriteLine(currPos);
                if(Map[SandSource.x, SandSource.y] == Entity.Sand)
                {
                    break;
                }

                int down = currPos.y + 1;
                int right = currPos.x + 1;
                int left = currPos.x - 1;

                if(down >= height)
                {
                    break;
                }
                if(Map[currPos.x, down] < Entity.Sand) //if Air || SandSource
                {
                    currPos = (currPos.x, down);
                    continue;
                }

                if(left < 0)
                {
                    if(Expand)
                    {
                        Map = ExpandMap(Map, -1, 0);
                        currPos = (currPos.x + 1, currPos.y);
                        SandSource = (SandSource.x + 1, SandSource.y);
                        left++;
                        right++;
                        width++;
                    }
                    else
                    {
                        break;
                    }
                }
                if(Map[left, down] < Entity.Sand)
                {
                    currPos = (left, down);
                    continue;
                }

                if(right >= width)
                {
                    if(Expand)
                    {
                        Map = ExpandMap(Map, 1, 0);
                        width++;
                    }
                    else
                    {
                        break;
                    }
                }
                if(Map[right, down] < Entity.Sand)
                {
                    currPos = (right, down);
                    continue;
                }

                Map[currPos.x, currPos.y] = Entity.Sand;
                currPos = SandSource;
                sandUnits++;
            }

            PrintMap(Map);
            return sandUnits;
        }

        private static Entity[,] ExpandMap(Entity[,] Map, int xOffset, int yOffset)
        {
            int width = Map.GetLength(0);
            int height = Map.GetLength(1);

            Entity[,] newMap = new Entity[width + Math.Abs(xOffset), height + Math.Abs(yOffset)];
            xOffset = Math.Min(xOffset, 0);
            yOffset = Math.Min(yOffset, 0);

            for(int i = 0; i < width; i++)
            {
                for(int j = 0; j < height; j++)
                {
                    newMap[i - xOffset, j - yOffset] = Map[i, j];
                }
            }

            //floor
            width = newMap.GetLength(0);
            int y = newMap.GetLength(1) - 1;

            for(int x = 0; x < width; x++)
            {
                newMap[x, y] = Entity.Rock;
            }

            return newMap;
        }

        private static void AddLine(Entity[,] Map, (int first, int second) point, int otherPoint, bool isXLine)
        {
            if(point.first < point.second)
            {
                point = (point.second, point.first);
            }

            int diff = point.first - point.second;

            if(isXLine)
            {
                for(int i = 0; i <= diff; i++)
                {
                    Map[point.second + i, otherPoint] = Entity.Rock;
                }
            }
            else
            {
                for(int i = 0; i <= diff; i++)
                {
                    Map[otherPoint, point.second + i] = Entity.Rock;
                }
            }
        }

        private static void PrintMap(Entity[,] Map)
        {
            Dictionary<Entity, Char> entityDict = new() { { Entity.Air, '.' }, { Entity.Rock, '#' }, { Entity.Sand, 'o' }, { Entity.SandSource, '+' } };
            int width = Map.GetLength(0);
            int height = Map.GetLength(1);
            string s = "";
            for(int i = 0; i < height; i++)
            {
                for(int j = 0; j < width; j++)
                {
                    s += entityDict[Map[j, i]];
                }
                s += "\n";
            }
            Console.Clear();
            Console.WriteLine(s);
        }
    }
}
