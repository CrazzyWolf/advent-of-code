﻿using System.Diagnostics;

namespace Advent_of_Code
{
    internal class Day12
    {
        public static void Start()
        {
            string[] map = File.ReadAllLines("Day12/input.tar");
            int height = map.Length;
            int width = map[0].Length;
            int[,] grid = new int[height, width];
            (int y, int x, int counter) startPos = (0, 0, 0);
            (int y, int x) finishPos = (0, 0);
            List<(int y, int x, int counter)> lowestPoints = new();

            for(int i = 0; i < height; i++)
            {
                string line = map[i];
                for(int j = 0; j < width; j++)
                {
                    char c = line[j];
                    if((grid[i, j] = c - 'a') < 0)
                    {
                        if(c == 'S')
                        {
                            startPos = (i, j, 0);
                            grid[i, j] = 0;
                        }
                        else //'E'
                        {
                            finishPos = (i, j);
                            grid[i, j] = 25; // 'z' - 'a'
                        }
                    }
                    else if(c == 'a')
                    {
                        lowestPoints.Add((i, j, 0));
                    }
                }
            }

            Console.WriteLine(BFS(grid, startPos, finishPos));

            int fastestPath = int.MaxValue;
            foreach((int y, int x, int counter) start in lowestPoints)
            {
                fastestPath = Math.Min(fastestPath, BFS(grid, start, finishPos));
            }
            Console.WriteLine(fastestPath);
        }

        private static int BFS(int[,] Grid, (int y, int x, int counter) Start, (int y, int x) Finish)
        {
            int height = Grid.GetLength(0);
            int width = Grid.GetLength(1);

            HashSet<(int y, int x)> visited = new();
            Queue<(int y, int x, int counter)> queue = new();
            queue.Enqueue(Start);
            visited.Add((Start.y, Start.x));

            while(queue.Count > 0)
            {
                (int y, int x, int counter) currPos = queue.Dequeue();

                int y = currPos.y;
                int x = currPos.x;
                int counter = currPos.counter + 1;
                int nextLevel = Grid[y, x] + 1;

                List<(int y, int x)> positions = new();

                if(y > 0)
                {
                    positions.Add((y - 1, x));
                }

                if(y < height - 1)
                {
                    positions.Add((y + 1, x));
                }

                if(x > 0)
                {
                    positions.Add((y, x - 1));
                }

                if(x < width - 1)
                {
                    positions.Add((y, x + 1));
                }

                foreach((int y, int x) p in positions)
                {
                    if(!visited.Contains(p) && Grid[p.y, p.x] <= nextLevel)
                    {
                        if(p == Finish)
                        {
                            return counter;
                        }
                        else
                        {
                            queue.Enqueue((p.y, p.x, counter));
                            visited.Add(p);
                        }
                    }
                }
            }

            return int.MaxValue;
        }
    }
}
