﻿namespace Advent_of_Code
{
    internal class Day5
    {
        public static void Start()
        {
            string[] lines = File.ReadAllLines("Day5/input.tar");
            int i = Array.FindIndex(lines, l => l == "");
            string line;

            int numOfStacks = int.Parse(lines[i - 1].Trim().Split(" ").Last());

            Stack<char>[] creates = new Stack<char>[numOfStacks];
            Stack<char>[] creates2 = new Stack<char>[numOfStacks];
            for(int j = 0; j < numOfStacks; j++)
            {
                creates[j] = new();
                creates2[j] = new();
            }

            for(int j = i - 2; j >= 0; j--)
            {
                line = lines[j];
                for(int k = 0; k < numOfStacks; k++)
                {
                    char c = line[1 + k * 4];
                    if(c != ' ')
                    {
                        creates[k].Push(c);
                        creates2[k].Push(c);
                    }
                }
            }

            for(int j = i + 1; j < lines.Length; j++)
            {
                string[] parts = lines[j].Split(' ');
                int move = int.Parse(parts[1]);
                int from = int.Parse(parts[3]) - 1;
                int to = int.Parse(parts[5]) - 1;

                char[] temp = new char[move];
                Stack<char> fromCreates = creates[from];
                Stack<char> fromCreates2 = creates2[from];
                Stack<char> toCreates = creates[to];
                Stack<char> toCreates2 = creates2[to];

                for(int k = 0; k < move; k++)
                {
                    toCreates.Push(fromCreates.Pop());
                    temp[k] = fromCreates2.Pop();
                }

                for(int k = move - 1; k >= 0; k--)
                {
                    toCreates2.Push(temp[k]);
                }
            }

            Console.WriteLine(string.Join("", creates.Select(x => x.First())));
            Console.WriteLine(string.Join("", creates2.Select(x => x.First())));
        }
    }
}
