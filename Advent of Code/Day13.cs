﻿namespace Advent_of_Code
{
    internal class Day13
    {
        public static void Start()
        {
            string[] input = File.ReadAllLines("Day13/input.tar");

            int sum = 0;

            List<List<object>> packets = new();

            for(int i = 0; i < input.Length; i += 3)
            {
                List<object> list1 = ParseList(input[i]);
                List<object> list2 = ParseList(input[i + 1]);

                packets.Add(list1);
                packets.Add(list2);

                if(CompareOrder(list1, list2) == 1)
                {
                    sum += 1 + i / 3;
                }
            }

            Console.WriteLine(sum);

            packets.Add(ParseList("[[2]]"));
            packets.Add(ParseList("[[6]]"));
            packets.Sort(CompareOrder);

            int key = 1;
            for(int i = 0; i < packets.Count; i++)
            {
                List<object> packet = packets[i];  
                if(packet.Count == 1 && packet.ElementAt(0) is List<object> list 
                    && list.Count == 1 && list.ElementAt(0) is int number && (number == 6 || number == 2))
                {
                    key *= packets.Count - i;
                }
            }

            Console.WriteLine(key);
        }

        private static int CompareOrder(List<object> list1, List<object> list2)
        {
            int count1 = list1.Count;
            int count2 = list2.Count;

            for(int i = 0; i < count1; i++)
            {
                if(count2 <= i)
                {
                    return -1;
                }

                object item1 = list1[i];
                object item2 = list2[i];
                int result = 0;

                if(item1 is int int1)
                {
                    if(item2 is int int2)
                    {
                        if(int1 > int2)
                        {
                            return -1;
                        }
                        else if(int1 < int2)
                        {
                            return 1;
                        }
                    }
                    else
                    {
                        result = CompareOrder(new List<object> { int1 }, (List<object>)item2);
                    }
                }
                else
                {
                    result = item2 is int int2
                        ? CompareOrder((List<object>)item1, new List<object> { int2 })
                        : CompareOrder((List<object>)item1, (List<object>)item2);
                }

                if(result != 0)
                {
                    return result;
                }
            }

            return count1 < count2 ? 1 : 0;
        }

        private static string ListToString(List<object> list, int level)
        {
            string s = "";
            foreach(object item in list)
            {
                if(item is List<object> subList)
                {
                    s += ListToString(subList, level + 1) + ",";
                }
                else if(item is int)
                {
                    s += item + ",";
                }
            }
            return "[" + (s.Length > 0 ?  s[0..^1] : s) + "]";
        }

        private static void OutputList(List<object> list, int level)
        {
            for(int j = 0; j < level; j++)
            {
                Console.Write("--->");
            }
            Console.WriteLine("List level {0}:", level);
            foreach(object item in list)
            {
                if(item is List<object> subList)
                {
                    OutputList(subList, level + 1);
                }
                else if(item is int)
                {
                    for(int j = 0; j < level + 1; j++)
                    {
                        Console.Write("--->");
                    }
                    Console.WriteLine(item);
                }
            }
        }

        private static List<object> ParseList(string s)
        {
            List<object> result = new();
            List<char> buffer = new();
            string curr = s[1..^1];
            int level = 0;

            for(int i = 0; i < curr.Length; i++)
            {
                char c = curr[i];
                if(level == 0 && c == ',')
                {
                    BufferToResult(result, buffer, false);
                }
                else
                {
                    buffer.Add(c);
                    if(c == '[')
                    {
                        level++;
                    }
                    else if(c == ']')
                    {
                        level--;
                        if(level == 0)
                        {
                            BufferToResult(result, buffer, true);
                        }
                    }
                }
            }

            BufferToResult(result, buffer, false);

            return result;
        }

        private static void BufferToResult(List<object> result, List<char> buffer, bool isList)
        {
            if(buffer.Count > 0)
            {
                string s = new(buffer.ToArray());
                result.Add(isList ? ParseList(s) : int.Parse(s));
                buffer.Clear();
            }
        }
    }
}
