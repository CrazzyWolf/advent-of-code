﻿namespace Advent_of_Code
{
    internal class Day19
    {
        private static Dictionary<string, int> Inventory = new();

        public static void Start()
        {
            string[] input = File.ReadAllLines("Day19/test.txt");

            Dictionary<string, string[]>[] blueprints = new Dictionary<string, string[]>[input.Length];

            for(int i = 0; i < input.Length; i++)
            {
                string line = input[i];
                string[] split = line.Split('.');
                var blueprint = blueprints[i] = new();

                for(int j = 0; j < split.Length - 1; j++)
                {
                    string s = split[j];
                    blueprint[s.Split("Each ")[1].Split(" robot")[0]] = s.Split("costs ")[1].Split(" and ");
                }
            }

            foreach(Dictionary<string, string[]> blueprint in blueprints)
            {
                int geodes = 0;
                for(int minute = 1; minute <= 24; minute++)
                {
                    if(HaveResources(blueprint["geode"]))
                    {

                    }

                    //if(Inventory.ContainsKey(whatToMine))
                    {

                    }

                }
            }
        }

        private static bool HaveResources(string[] requirements)
        {
            foreach(string requirement in requirements)
            {

            }
        }
    }
}
