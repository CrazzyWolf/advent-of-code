﻿namespace Advent_of_Code
{
    internal class Day7
    {
        private static readonly Dictionary<string, List<string>> Directories = new();
        private static readonly Dictionary<string, int> DirectorySizes = new();

        public static void Start()
        {
            string[] input = File.ReadAllLines("Day7/input.tar").Skip(1).ToArray();
            int dirSum = 0;
            string currDirString = "/";
            List<string> currDir = new();

            Directories.Add("/", currDir);
            foreach(string line in input)
            {
                if(line[0] == '$')
                {
                    if(line == "$ cd ..")
                    {
                        currDirString = currDirString[..currDirString.LastIndexOf("/")];
                    }
                    else if(line != "$ ls")
                    {
                        currDirString += "/" + line.Split(" ")[2];
                        currDir = new();
                        Directories.Add(currDirString, currDir);
                    }
                }
                else
                {
                    currDir.Add(line);
                }
            }

            foreach(var (dir, list) in Directories)
            {
                int size = GetDirSize(dir);
                if(size <= 100000)
                {
                    dirSum += size;
                }
            }

            int freeSpaceNeeded = GetDirSize("/") - 40000000;
            int dirSize = int.MaxValue;
            
            foreach(var (dir, size) in DirectorySizes)
            {
                if(size >= freeSpaceNeeded)
                {
                    dirSize = Math.Min(dirSize, size);
                }
            }

            Console.WriteLine(dirSum);
            Console.WriteLine(dirSize);
        }

        private static int GetDirSize(string Dir)
        {
            List<string> list = Directories[Dir];
            int dirSize = 0;
            foreach(string line in list)
            {
                if(line.StartsWith("dir "))
                {
                    string dir = Dir + "/" + line.Split(" ")[1];
                    dirSize += DirectorySizes.ContainsKey(dir) ? DirectorySizes[dir] : (DirectorySizes[dir] = GetDirSize(dir));
                }
                else
                {
                    dirSize += int.Parse(line.Split(" ")[0]);
                }
            }

            return dirSize;
        }
    }
}
