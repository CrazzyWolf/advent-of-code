﻿namespace Advent_of_Code
{
    internal class Day3
    {
        public static void Start()
        {
            int Sum = 0;

            foreach(string Line in File.ReadAllLines("Day3/input.txt"))
            {
                Sum += ConvertChar(FindDuplicate(Line));
            }

            Console.WriteLine(Sum);
        }

        private static int FindDuplicate(string Line)
        {
            int Half = Line.Length / 2;
            for(int i = 0; i < Half; i++)
            {
                char Item = Line[i];
                for(int j = Half; j < Line.Length; j++)
                {
                    if(Item == Line[j])
                    {
                        return Item;
                    }
                }
            }
            throw new Exception("No duplicate");
        }

        public static void Start2()
        {
            int ElfNumber = 2;
            string[] LineArray = new string[ElfNumber];
            int LineNumber = 0;
            int Sum = 0;

            foreach(string Line in File.ReadAllLines("Day3/input.txt"))
            {
                if(LineNumber < ElfNumber)
                {
                    LineArray[LineNumber] = Line;
                }
                else
                {
                    foreach(char Char in Line)
                    {
                        bool Correct = true;

                        foreach(string CurrLine in LineArray)
                        {
                            if(!CurrLine.Contains(Char))
                            {
                                Correct = false;
                                break;
                            }
                        }

                        if(Correct)
                        {
                            Sum += ConvertChar(Char);
                            break;
                        }
                    }
                    LineNumber = -1;
                }

                LineNumber++;
            }

            Console.WriteLine(Sum);
        }

        private static int ConvertChar(int c)
        {
            return c - (c < 'a' ? 38 : 96); //a - 1 = -96 offset (0 - 26), A - 1 + 26 = -38 offset (27 - 52)
        }
    }
}
