﻿namespace Advent_of_Code
{
    internal class Day2
    {
        public static void Start()
        {
            string[] Lines = File.ReadAllLines("Day2/input.tar");

            int TotalScore = 0;

            Dictionary<string, string> MeDict = new();
            MeDict["X"] = "A";
            MeDict["Y"] = "B";
            MeDict["Z"] = "C";

            Dictionary<string, int> ValueDict = new();
            ValueDict["A"] = 1;
            ValueDict["B"] = 2;
            ValueDict["C"] = 3;

            foreach(string Line in Lines)
            {
                string[] Split = Line.Split(' ');
                string Opponent = Split[0];
                string Me = MeDict[Split[1]];

                TotalScore += ValueDict[Me];

                if(Opponent == Me) //Tie
                {
                    TotalScore += 3;
                }
                else if(Opponent == "A") //Rock
                {
                    if(Me == "B")
                    {
                        TotalScore += 6;
                    }
                }
                else if(Opponent == "B") //Paper
                {
                    if(Me == "C")
                    {
                        TotalScore += 6;
                    }
                }
                else if(Opponent == "C") //Scissors
                {
                    if(Me == "A")
                    {
                        TotalScore += 6;
                    }
                }
            }

            Console.WriteLine(TotalScore);
        }
        public static void Start2()
        {
            string[] Lines = File.ReadAllLines("Day2/input.tar");

            int TotalScore = 0;

            Dictionary<string, int> ValueDict = new(); //Offset -1
            ValueDict["A"] = 0; //Rock
            ValueDict["B"] = 1; //Paper
            ValueDict["C"] = 2; //Scissors

            Dictionary<string, int> ResultDict = new(); //Offset +1
            ResultDict["X"] = 1; //Lose
            ResultDict["Y"] = 4; //Tie
            ResultDict["Z"] = 7; //Win

            Dictionary<string, int> OffsetDict = new();
            OffsetDict["X"] = 2; //Lose
            OffsetDict["Y"] = 0; //Tie
            OffsetDict["Z"] = 1; //Win

            int Count = ValueDict.Count;

            foreach(string Line in Lines)
            {
                string[] Split = Line.Split(' ');
                string Opponent = Split[0];
                string Result = Split[1];

                TotalScore += (ValueDict[Opponent] + OffsetDict[Result]) % Count + ResultDict[Result];
            }

            Console.WriteLine(TotalScore);
        }
    }
}
