﻿namespace Advent_of_Code
{
    internal class Day8
    {
        public static void Start()
        {
            string[] treeRows = File.ReadAllLines("Day8/input.tar");
            int size = treeRows.Length;
            int innerSize = size - 1;
            int visibleSum = innerSize * 4;
            int[,] treeGrid = new int[size, size];
            int[,] leftGrid = new int[size, size];
            int[,] rightGrid = new int[size, size];
            int[,] topGrid = new int[size, size];
            int[,] bottomGrid = new int[size, size];

            for(int i = 0; i < size; i++)
            {
                string treeRow = treeRows[i];
                for(int j = 0; j < size; j++)
                {
                    treeGrid[i, j] = leftGrid[i, j] = rightGrid[i, j] = topGrid[i, j] = bottomGrid[i, j] = treeRow[j] - '0';
                }
            }

            int maxScenicScore = 0;
            for(int i = 1; i < innerSize; i++)
            {
                for(int j = 1; j < innerSize; j++)
                {
                    int scoreTop = 0;
                    int scoreBottom = 0;
                    int scoreLeft = 0;
                    int scoreRight = 0;

                    for(int k = i - 1; k >= 0; k--)
                    {
                        scoreTop++;
                        if(treeGrid[i, j] <= treeGrid[k, j])
                        {
                            break;
                        }
                    }
                    for(int k = i + 1; k < size; k++)
                    {
                        scoreBottom++;
                        if(treeGrid[i, j] <= treeGrid[k, j])
                        {
                            break;
                        }
                    }
                    for(int k = j - 1; k >= 0; k--)
                    {
                        scoreLeft++;
                        if(treeGrid[i, j] <= treeGrid[i, k])
                        {
                            break;
                        }
                    }
                    for(int k = j + 1; k < size; k++)
                    {
                        scoreRight++;
                        if(treeGrid[i, j] <= treeGrid[i, k])
                        {
                            break;
                        }
                    }

                    maxScenicScore = Math.Max(maxScenicScore, scoreLeft * scoreRight * scoreTop * scoreBottom);
                }
            }


            for(int i = 0; i < size; i++)
            {
                for(int j = 1; j < size; j++)
                {
                    leftGrid[i, j] = Math.Max(leftGrid[i, j], leftGrid[i, j - 1]);
                }
            }


            for(int i = 1; i < size; i++)
            {
                for(int j = 0; j < size; j++)
                {
                    topGrid[i, j] = Math.Max(topGrid[i, j], topGrid[i - 1, j]);
                }
            }


            for(int i = 0; i < size; i++)
            {
                for(int j = size - 2; j >= 0; j--)
                {
                    rightGrid[i, j] = Math.Max(rightGrid[i, j], rightGrid[i, j + 1]);
                }
            }


            for(int i = size - 2; i >= 0; i--)
            {
                for(int j = 0; j < size; j++)
                {
                    bottomGrid[i, j] = Math.Max(bottomGrid[i, j], bottomGrid[i + 1, j]);
                }
            }

            for(int i = 1; i < innerSize; i++)
            {
                for(int j = 1; j < innerSize; j++)
                {
                    if(treeGrid[i, j] > leftGrid[i, j - 1] || treeGrid[i, j] > rightGrid[i, j + 1] 
                        || treeGrid[i, j] > topGrid[i - 1, j] || treeGrid[i, j] > bottomGrid[i + 1, j])
                    {
                        visibleSum++;
                    }
                }
            }

            Console.WriteLine(visibleSum);
            Console.WriteLine(maxScenicScore);
        }
    }
}
