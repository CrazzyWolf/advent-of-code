﻿namespace Advent_of_Code
{
    internal class Day16
    {
        private static readonly Dictionary<string, (int flowRate, List<string> paths)> Spots = new();
        private static readonly Dictionary<(string, string), int> Distances = new();
        private static readonly List<string> NonZeroFlowSpots = new();

        public static void Start()
        {
            string[] input = File.ReadAllLines("Day16/input.tar");

            foreach(string line in input)
            {
                if(line.Length == 0)
                {
                    break;
                }

                string[] parts = line.Split(' ');

                List<string> paths = new();
                for(int i = 9; i < parts.Length; i++)
                {
                    paths.Add(parts[i][..2]);
                }

                int flow = int.Parse(parts[4][5..^1]);
                string valve = parts[1];
                Spots.Add(valve, (flow, paths));

                if(flow != 0)
                {
                    NonZeroFlowSpots.Add(valve);
                }
            }

            foreach(var spot in Spots)
            {
                if(spot.Value.flowRate != 0)
                {
                    string key = spot.Key;
                    Distances[("AA", key)] = BFS("AA", key);

                    foreach(var spot2 in Spots)
                    {
                        if(spot2.Value.flowRate != 0)
                        {
                            string key2 = spot2.Key;
                            if(key != key2)
                            {
                                Distances[(key, key2)] = Distances[(key2, key)] = BFS(key, key2);
                            }
                        }
                    }
                }
            }

            var allPaths = GetAllPaths(new() { (new() { "AA" }, 0) }, 30);

            Console.WriteLine(allPaths.Max(path => TryPath(path.pathList, 30)));

            var tempPathLists = GetAllPaths(new() { (new() { "AA" }, 0) }, 26);
            
            List<int> allPaths2 = new();

            Console.WriteLine(tempPathLists.Count);

            bool[,] pathTable = new bool[tempPathLists.Count, NonZeroFlowSpots.Count];

            for(int i = 0; i < tempPathLists.Count;i++)
            {
                var pathList = tempPathLists[i].pathList;
                allPaths2.Add(TryPath(pathList, 26));
                for(int j = 0; j < NonZeroFlowSpots.Count; j++)
                {
                    pathTable[i, j] = pathList.Contains(NonZeroFlowSpots[j]);
                }
            }

            int max = 0;
            for(int i = 0; i < allPaths2.Count; i++)
            {
                for(int j = 0; j < allPaths2.Count; j++)
                {
                    bool good = true;

                    for(int k = 0; k < NonZeroFlowSpots.Count; k++)
                    {
                        if(pathTable[i, k] && pathTable[j, k])
                        {
                            good = false;
                            break;
                        }
                    }

                    if(good)
                    {
                        max = Math.Max(max, allPaths2[i] + allPaths2[j]);
                    }
                }
            }

            Console.WriteLine(max);
        }


        private static List<(List<string> pathList, int fullDistance)> GetAllPaths(List<(List<string> pathList, int fullDistance)> oldPaths, int maxDistance)
        {
            List<(List<string> pathList, int fullDistance)> newPaths = new(oldPaths);
            int maxPathLength = NonZeroFlowSpots.Count + 1;

            foreach(var path in oldPaths)
            {
                if(path.fullDistance < maxDistance && path.pathList.Count != maxPathLength)
                {
                    bool canFitMore = false;

                    foreach(var spot in NonZeroFlowSpots)
                    {
                        if(!path.pathList.Contains(spot))
                        {
                            int newDistance = path.fullDistance + Distances[(path.pathList.Last(), spot)] + 1; //1 minute to turn on valve
                            if(newDistance <= maxDistance)
                            {
                                canFitMore = true;
                                List<(List<string> pathList, int fullDistance)> singlePath = new();
                                List<string> singlePathList = new(path.pathList);
                                singlePathList.Add(spot);
                                singlePath.Add((singlePathList, newDistance));
                                newPaths.AddRange(GetAllPaths(singlePath, maxDistance));
                            }
                        }
                    }

                    if(canFitMore)
                    {
                        newPaths.Remove(path);
                    }
                }
            }

            return newPaths;
        }

        private static int BFS(string start, string end)
        {
            HashSet<string> visited = new();
            Queue<List<string>> paths = new();

            foreach(string path in Spots[start].paths)
            {
                paths.Enqueue(new List<string>() { path });
            }

            while(paths.Count > 0)
            {
                List<string> pathList = paths.Dequeue();
                string last = pathList.Last();

                if(last == end)
                {
                    return pathList.Count;
                }

                visited.Add(last);
                foreach(string nextPath in Spots[last].paths)
                {
                    if(!visited.Contains(nextPath))
                    {
                        List<string> newList = new(pathList);
                        newList.Add(nextPath);
                        paths.Enqueue(newList);
                    }
                }

            }

            return -1;
        }
        private static int TryPath(List<string> path, int timeLeft)
        {
            int totalPressure = 0;
            int pressure = 0;
            string currPlace = path.First();
            bool opening = false;
            int listIndex = 1;
            int distanceRemaining = Distances[(currPlace, path[listIndex])];
            int pathCount = path.Count;

            for(int time = 1; time <= timeLeft; time++)
            {
                if(opening)
                {
                    opening = false;
                    pressure += Spots[currPlace].flowRate;
                    distanceRemaining = (listIndex < pathCount ? Distances[(currPlace, path[listIndex])] : 0) - 1;
                }
                else if(distanceRemaining == 0)
                {
                    currPlace = path[listIndex++];
                    opening = true;
                }
                else //both walking and standing
                {
                    distanceRemaining--;
                }

                totalPressure += pressure;
            }

            return totalPressure;
        }
    }
}
