﻿namespace Advent_of_Code
{
    internal class Day18
    {
        private static readonly HashSet<(int x, int y, int z)> cubes = new();
        private static readonly HashSet<(int x, int y, int z)> trappedSpace = new();
        private static (int x, int y, int z) min = (int.MaxValue, int.MaxValue, int.MaxValue);
        private static (int x, int y, int z) max = (int.MinValue, int.MinValue, int.MinValue);

        public static void Start()
        {
            string[] input = File.ReadAllLines("Day18/input.txt");


            foreach(string line in input)
            {
                if(line.Length == 0)
                {
                    break;
                }

                int[] split = line.Split(',').Select(s => int.Parse(s)).ToArray();
                int x = split[0], y = split[1], z = split[2];
                cubes.Add((x, y, z));

                min.x = Math.Min(min.x, x);
                min.y = Math.Min(min.y, y);
                min.z = Math.Min(min.z, z);

                max.x = Math.Max(max.x, x);
                max.y = Math.Max(max.y, y);
                max.z = Math.Max(max.z, z);
            }

            int surfaceArea = 0;
            foreach(var cube in cubes)
            {
                foreach(var side in GetSides(cube))
                {
                    if(!cubes.Contains(side))
                    {
                        surfaceArea++;
                    }
                }
            }

            Console.WriteLine(surfaceArea);

            for(int x = min.x; x <= max.x; x++)
            {
                for(int y = min.y; y <= max.y; y++)
                {
                    for(int z = min.z; z <= max.z; z++)
                    {
                        var cube = (x, y, z);
                        if(!cubes.Contains(cube) && IsTrapped(cube))
                        {
                            trappedSpace.Add(cube);
                            
                            foreach(var side in GetSides(cube))
                            {
                                if(cubes.Contains(side))
                                {
                                    surfaceArea--;
                                }
                            }
                        }
                    }
                }
            }

            Console.WriteLine(surfaceArea);
        }

        private static (int x, int y, int z)[] GetSides((int x, int y, int z) cube) => new (int, int, int)[6]
        {
            (cube.x + 1, cube.y, cube.z),
            (cube.x - 1, cube.y, cube.z),
            (cube.x, cube.y + 1, cube.z),
            (cube.x, cube.y - 1, cube.z),
            (cube.x, cube.y, cube.z + 1),
            (cube.x, cube.y, cube.z - 1)
        };

        private static bool IsTrapped((int x, int y, int z) cube)
        {
            Stack<(int x, int y, int z)> stack = new();
            HashSet<(int x, int y, int z)> visited = new();
            stack.Push(cube);
            visited.Add(cube);

            while(stack.Count > 0)
            {
                var currCube = stack.Pop();

                int x = currCube.x, y = currCube.y, z = currCube.z;
                if(x == min.x || x == max.x || y == min.y || y == max.y || z == min.z || z == max.z)
                {
                    return false; //edge of map is not trapped
                }

                foreach(var side in GetSides(currCube))
                {
                    if(trappedSpace.Contains(side))
                    {
                        return true; //if neighbour is trapped then current is too
                    }
                    if(!cubes.Contains(side) && !visited.Contains(side))
                    {
                        stack.Push(side);
                        visited.Add(side);
                    }
                }
            }

            return true;
        }
    }
}
